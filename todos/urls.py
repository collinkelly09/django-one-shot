from django.urls import path
from todos import views

urlpatterns = [
    path('todos/', views.todo_list_list, name='todo_list_list'),
    path('todos/<int:id>/', views.todo_list_detail, name='todo_list_detail'),
    path('todos/create/', views.todo_list_create, name='todo_list_create'),
    path('todos/<int:id>/edit/', views.todo_list_update, name='todo_list_update'),
    path('todos/<int:id>/delete/', views.todo_list_delete, name='todo_list_delete'),
    path('todos/items/create/', views.todo_item_create, name='todo_item_create'),
    path('todos/items/<int:id>/edit/', views.todo_item_update, name='todo_item_update'),
]
